﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CE.Entity
{
    public class ConsumedBeer
    {
        [Key]
        public Guid ConsumedBeerId { get; set; }

        //This Id is belongs to user
        public string Id { get; set; }
        [ForeignKey("Id")]
        public virtual User User { get; set; }

        public Guid BeerId { get; set; }
        [ForeignKey("BeerId")]
        public virtual Beer Beer { get; set; }
    }
}
