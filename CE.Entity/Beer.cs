﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CE.Entity
{
    public class Beer
    {
        [Key]
        public Guid BeerId { get; set; }

        public string Manufacturer { get; set; }

        public string Name { get; set; }

        public Guid CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public string Country { get; set; }

        public string Price { get; set; }

        public string Description { get; set; }

        public bool IsArchive { get; set; }
    }
}
