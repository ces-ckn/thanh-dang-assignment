﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.Common
{
    public class Constants
    {
        public const string AppConnection = "CodeEngineConnection";

        public struct ApplicationRole
        {
            public const string Administrator = "Administrator";
            public const string Customer = "Customer";
        }
    }
}
