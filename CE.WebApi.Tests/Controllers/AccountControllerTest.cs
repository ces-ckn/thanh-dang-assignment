﻿using CE.WebApi.Controllers;
using CE.WebApi.Models;
using CE.WebApi.Tests.App_Start;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Ninject;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CE.WebApi.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        private AccountController controller;
        private string host = "http://codeengine.apphb.com/";

        private string adminUserName = "admin@gmail.com";
        private string adminPassword = "abcde12345-";

        private string customerUserName = "customer@gmail.com";
        private string customerPassword = "abcde12345-";

        [TestInitialize]
        public void MyTestInitialize()
        {
            var kernel = NinjectWebCommon.CreatePublicKernel();
            controller = kernel.Get<AccountController>();
        }

        [TestMethod]
        public void RegisterAsAdmin()
        {
            // Arrange
            RegisterBindingModel model = new RegisterBindingModel()
                {
                    Email = adminUserName,
                    Password = adminPassword,
                    ConfirmPassword = adminPassword,
                };

            var result = controller.RegisterAsAdmin(model).Result;

            bool isExist = controller._userSqlServerRepository.CheckExistEmail(model.Email);

            // Assert
            Assert.IsTrue(isExist);
        }

        [TestMethod]
        public void RegisterAsCustomer()
        {
            // Arrange
            RegisterBindingModel model = new RegisterBindingModel()
            {
                Email = customerUserName,
                Password = customerPassword,
                ConfirmPassword = customerPassword,
            };

            var result = controller.RegisterAsCustomer(model).Result;

            bool isExist = controller._userSqlServerRepository.CheckExistEmail(model.Email);

            // Assert
            Assert.IsTrue(isExist);
        }

        [TestMethod]
        public void LoginAsAdminAndGetSomething()
        {
            string adminUserToken = GetToken(adminUserName, adminPassword);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", adminUserToken);
            var response = client.GetAsync(host + "api/Beer/Get").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IndexOf("Authorization") < 0);
        }

        [TestMethod]
        public void LoginAsCustomerAndGetSomething()
        {
            string customerUserToken = GetToken(customerUserName, customerPassword);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", customerUserToken);
            var response = client.GetAsync(host + "api/Beer/Get").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IndexOf("Authorization") >= 0);
        }

        private string GetToken(string userName, string password)
        {
            HttpClient client = new HttpClient();
            var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>( "grant_type", "password" ), 
                        new KeyValuePair<string, string>( "username", userName ), 
                        new KeyValuePair<string, string>( "Password", password )
                    };
            var content = new FormUrlEncodedContent(pairs);

            // Attempt to get a token from the token endpoint of the Web Api host:
            HttpResponseMessage response = client.PostAsync(host + "Token", content).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // De-Serialize into a dictionary and return:
            Dictionary<string, string> tokenDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
            return tokenDictionary["access_token"];
        }
    }
}
