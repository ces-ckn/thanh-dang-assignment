﻿using AutoMapper;
using CE.Entity;
using CE.WebApi.Controllers;
using CE.WebApi.Models;
using CE.WebApi.Tests.App_Start;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Results;

namespace CE.WebApi.Tests.Controllers
{
    [TestClass]
    public class BeerControllerTest
    {
        private BeerController beerController;
        private CategoryController categoryController;
        private string host = "http://codeengine.apphb.com/";

        [TestInitialize]
        public void MyTestInitialize()
        {
            var kernel = NinjectWebCommon.CreatePublicKernel();
            beerController = kernel.Get<BeerController>();
            categoryController = kernel.Get<CategoryController>();

            Mapper.CreateMap<Beer, BeerViewModel>()
                .ForMember(d => d.CategoryViewModel, m => m.MapFrom(s => s.Category));
            Mapper.CreateMap<BeerViewModel, Beer>()
                .ForMember(d => d.Category, m => m.MapFrom(s => s.CategoryViewModel));
            Mapper.CreateMap<Category, CategoryViewModel>()
                .ForMember(d => d.BeerViewModelList, m => m.MapFrom(s => s.BeerList));
            Mapper.CreateMap<CategoryViewModel, Category>()
                .ForMember(d => d.BeerList, m => m.MapFrom(s => s.BeerViewModelList));
        }

        [TestMethod]
        public void GetByCategory()
        {
            // Arrange
            var category = DummyCategory();
            categoryController.Post(category);

            var beerViewModel = DummyBeer(category.CategoryId);
            beerController.Post(beerViewModel);

            // Act
            IEnumerable<BeerViewModel> result = beerController.GetAnonymous(category.Name);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any(r => r.BeerId == beerViewModel.BeerId));
        }

        [TestMethod]
        public void Get()
        {
            // Act
            IEnumerable<BeerViewModel> result = beerController.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count() >= 0);
        }

        [TestMethod]
        public void GetById()
        {
            // Arrange
            var category = DummyCategory();
            categoryController.Post(category);

            var beerViewModel = DummyBeer(category.CategoryId);
            beerController.Post(beerViewModel);

            // Act
            BeerViewModel result = beerController.Get(beerViewModel.BeerId);

            // Assert
            Assert.AreEqual(beerViewModel.BeerId, result.BeerId);
        }

        [TestMethod]
        public void Post()
        {
            // Arrange
            var category = DummyCategory();
            categoryController.Post(category);

            // Act
            var beerViewModel = DummyBeer(category.CategoryId);
            var result = beerController.Post(beerViewModel);

            // Assert
            //Assert.IsInstanceOfType(result, typeof(OkResult));
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Archive()
        {
            // Insert category and beer
            var category = DummyCategory();
            categoryController.Post(category);

            var beerViewModel = DummyBeer(category.CategoryId);
            beerController.Post(beerViewModel);

            // Act
            ArchiveBeerViewModel archiveBeer = new ArchiveBeerViewModel() { BeerId = beerViewModel.BeerId };
            beerController.Archive(archiveBeer);
            BeerViewModel result = beerController.Get(beerViewModel.BeerId);

            // Assert
            Assert.IsTrue(result.IsArchive == true);
        }

        [TestMethod]
        public void Delete()
        {
            // Insert category and beer
            var category = DummyCategory();
            categoryController.Post(category);

            var beerViewModel = DummyBeer(category.CategoryId);
            beerController.Post(beerViewModel);

            // Act
            var result = beerController.Delete(beerViewModel.BeerId);

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        [TestMethod]
        public void GetAnonymous()
        {
            // Insert category and beer
            var category = DummyCategory();
            categoryController.Post(category);

            var beerViewModel = DummyBeer(category.CategoryId);
            beerController.Post(beerViewModel);

            // Get beer list by anonymous
            HttpClient client = new HttpClient();
            var response = client.GetAsync(host + "api/Beer/GetAnonymous").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }

        private CategoryViewModel DummyCategory()
        {
            Random rnd = new Random();
            return new CategoryViewModel()
            {
                CategoryId = Guid.NewGuid(),
                Name = "Category " + rnd.Next()
            };
        }

        private BeerViewModel DummyBeer(Guid categoryId)
        {
            Random rnd = new Random();
            return new BeerViewModel()
            {
                BeerId = Guid.NewGuid(),
                Manufacturer = "Manufacturer",
                Name = "Name " + rnd.Next(),
                CategoryId = categoryId,
                Country = "Country",
                Price = "Price",
                Description = "Description",
            };
        }
    }
}
