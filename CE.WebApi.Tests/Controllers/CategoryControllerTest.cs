﻿using AutoMapper;
using CE.Entity;
using CE.WebApi.Controllers;
using CE.WebApi.Models;
using CE.WebApi.Tests.App_Start;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;

namespace CE.WebApi.Tests.Controllers
{
    [TestClass]
    public class CategoryControllerTest
    {
        private BeerController beerController;
        private CategoryController categoryController;

        [TestInitialize]
        public void MyTestInitialize()
        {
            var kernel = NinjectWebCommon.CreatePublicKernel();
            beerController = kernel.Get<BeerController>();
            categoryController = kernel.Get<CategoryController>();

            Mapper.CreateMap<Beer, BeerViewModel>()
                .ForMember(d => d.CategoryViewModel, m => m.MapFrom(s => s.Category));
            Mapper.CreateMap<BeerViewModel, Beer>()
                .ForMember(d => d.Category, m => m.MapFrom(s => s.CategoryViewModel));
            Mapper.CreateMap<Category, CategoryViewModel>()
                .ForMember(d => d.BeerViewModelList, m => m.MapFrom(s => s.BeerList));
            Mapper.CreateMap<CategoryViewModel, Category>()
                .ForMember(d => d.BeerList, m => m.MapFrom(s => s.BeerViewModelList));
        }

        [TestMethod]
        public void Get()
        {
            // Act
            IEnumerable<CategoryViewModel> result = categoryController.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count() >= 0);
        }

        [TestMethod]
        public void GetById()
        {
            // Arrange
            var category = DummyCategory();
            categoryController.Post(category);

            // Act
            CategoryViewModel result = categoryController.Get(category.CategoryId);

            // Assert
            Assert.AreEqual(category.CategoryId, result.CategoryId);
        }

        [TestMethod]
        public void Post()
        {
            // Arrange
            var category = DummyCategory();

            // Act
            var result = categoryController.Post(category);

            // Assert
            //Assert.IsInstanceOfType(result, typeof(OkResult));
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Delete()
        {
            // Arrange
            var category = DummyCategory();
            categoryController.Post(category);

            // Act
            var result = categoryController.Delete(category.CategoryId);

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        [TestMethod]
        public void DeleteCategoryHasBeer()
        {
            // Arrange
            var category = DummyCategory();
            categoryController.Post(category);

            var beerViewModel = DummyBeer(category.CategoryId);
            beerController.Post(beerViewModel);

            // Act
            var result = categoryController.Delete(category.CategoryId);

            // Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        private CategoryViewModel DummyCategory()
        {
            Random rnd = new Random();
            return new CategoryViewModel()
              {
                  CategoryId = Guid.NewGuid(),
                  Name = "Category " + rnd.Next()
              };
        }

        private BeerViewModel DummyBeer(Guid categoryId)
        {
            Random rnd = new Random();
            return new BeerViewModel()
              {
                  BeerId = Guid.NewGuid(),
                  Manufacturer = "Manufacturer",
                  Name = "Name " + rnd.Next(),
                  CategoryId = categoryId,
                  Country = "Country",
                  Price = "Price",
                  Description = "Description",
              };
        }
    }
}
