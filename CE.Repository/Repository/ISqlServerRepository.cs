﻿using System.Data.Entity;

namespace CE.Repository
{
    public interface ISqlServerRepository<T> : IBaseRepository<T> where T : class
    {
        DbContext _dataContext { get; set; }
        IDbSet<T> _dbSet { get; set; }
    }
}
