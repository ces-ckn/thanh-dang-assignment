﻿using CE.Entity;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace CE.Repository
{
    public interface IUserSqlServerRepository : ISqlServerRepository<User>
    {
        Task<IdentityResult> Register(User user);

        Task<User> FindUserByIdAsync(string userId);

        Task<Role> FindRoleByNameAsync(string roleName);

        Task<IdentityResult> AddToRoleAsync(string userId, string roleName);

        bool CheckExistEmail(string email);
    }
}
