﻿using CE.Entity;
using CE.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace CE.Repository
{
    public class UserSqlServerRepository : SqlServerRepository<User>, IUserSqlServerRepository
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public UserSqlServerRepository(DbContext dbContext,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
            : base(dbContext)
        {
            _dataContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
            //_userManager = new ApplicationUserManager(new UserStore<User>(dbContext));
            //_roleManager = new ApplicationRoleManager(new RoleStore<Role>(dbContext));
        }

        public async Task<IdentityResult> Register(User user)
        {
            return await _userManager.CreateAsync(user, user.Password).ConfigureAwait(true);
        }

        public async Task<User> FindUserByIdAsync(string userId)
        {
            return await _userManager.FindByIdAsync(userId).ConfigureAwait(true);
        }

        public async Task<Role> FindRoleByNameAsync(string roleName)
        {
            return await _roleManager.FindByNameAsync(roleName).ConfigureAwait(true);
        }

        public async Task<IdentityResult> AddToRoleAsync(string userId, string roleName)
        {
            return await _userManager.AddToRoleAsync(userId, roleName).ConfigureAwait(true);
        }

        public bool CheckExistEmail(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                var user = _userManager.FindByEmail(email);
                return user != null;
            }

            return true;
        }
    }
}
