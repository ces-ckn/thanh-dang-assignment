﻿using CE.Common;
using CE.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CE.Repository
{
    public class SqlServerDbContext : IdentityDbContext<User>
    {
        public SqlServerDbContext()
            : base(Constants.AppConnection)
        {
        }

        public static SqlServerDbContext Create()
        {
            return new SqlServerDbContext();
        }

        IDbSet<Category> Categories { get; set; }
        IDbSet<Beer> Beers { get; set; }
        IDbSet<ConsumedBeer> ConsumedBeers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer<DatabaseContext>(null);

            //Remove unused conventions
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUser>().ToTable("Users");
            modelBuilder.Entity<User>().ToTable("Users");

            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<Role>().ToTable("Roles");

            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
        }
    }
}
