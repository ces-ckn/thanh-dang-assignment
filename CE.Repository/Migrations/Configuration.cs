namespace CE.Repository.Migrations
{
    using CE.Entity;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CE.Repository.SqlServerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CE.Repository.SqlServerDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            var userManager = new UserManager<User>(new UserStore<User>(context));
            var roleManager = new RoleManager<Role>(new RoleStore<Role>(context));

            if (!roleManager.RoleExists(Common.Constants.ApplicationRole.Administrator))
            {
                context.Roles.AddOrUpdate(new Role() { Name = Common.Constants.ApplicationRole.Administrator, Description = Common.Constants.ApplicationRole.Administrator });
            }
            if (!roleManager.RoleExists(Common.Constants.ApplicationRole.Customer))
            {
                context.Roles.AddOrUpdate(new Role() { Name = Common.Constants.ApplicationRole.Customer, Description = Common.Constants.ApplicationRole.Customer });
            }

            ////create user administrator
            //var admin = userManager.FindByName("admin@gmail.com");
            //if (admin == null)
            //{
            //    admin = new User();
            //    admin.UserName = "admin@gmail.com";
            //    admin.Email = admin.UserName;
            //    admin.PhoneNumber = "12345678";
            //    var result = userManager.CreateAsync(admin, "abcde12345-");
            //    if (result != null && result.Result.Succeeded)
            //    {
            //        //assgin to role
            //        var roleadmin = roleManager.FindByName("Administrator");
            //        if (roleadmin != null)
            //            userManager.AddToRoleAsync(admin.Id, "Administrator");
            //    }
            //}
        }
    }
}
