namespace CE.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nhu : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConsumedBeers",
                c => new
                    {
                        ConsumedBeerId = c.Guid(nullable: false),
                        Id = c.String(maxLength: 128),
                        BeerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ConsumedBeerId)
                .ForeignKey("dbo.Beers", t => t.BeerId)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.BeerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConsumedBeers", "Id", "dbo.Users");
            DropForeignKey("dbo.ConsumedBeers", "BeerId", "dbo.Beers");
            DropIndex("dbo.ConsumedBeers", new[] { "BeerId" });
            DropIndex("dbo.ConsumedBeers", new[] { "Id" });
            DropTable("dbo.ConsumedBeers");
        }
    }
}
