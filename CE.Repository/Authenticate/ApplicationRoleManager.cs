﻿using CE.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace CE.Repository
{
    public class ApplicationRoleManager : RoleManager<Role>
    {
        public ApplicationRoleManager(IRoleStore<Role, string> store)
            : base(store)
        {
        }
        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            //var roleStore = new RoleStore<IdentityRole>(context.Get<AuthenticationDbContext>());
            var roleStore = new RoleStore<Role>(context.Get<SqlServerDbContext>());
            return new ApplicationRoleManager(roleStore);
        }
    }
}
