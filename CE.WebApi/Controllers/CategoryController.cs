﻿using AutoMapper;
using CE.Entity;
using CE.Repository;
using System;
using System.Collections.Generic;
using System.Web.Http;
using CE.WebApi.Models;
using System.Linq;

namespace CE.WebApi.Controllers
{
    [Authorize(Roles = Common.Constants.ApplicationRole.Administrator)]
    public class CategoryController : BaseApiController
    {
        public CategoryController(ISqlServerRepository<Category> categoryRepository, ISqlServerRepository<Beer> beerRepository, ISqlServerRepository<ConsumedBeer> consumedRepository)
            : base(categoryRepository, beerRepository, consumedRepository)
        {
        }

        // GET api/values
        public IEnumerable<CategoryViewModel> Get()
        {
            IEnumerable<Category> categoryList = CategoryRepository.GetAll;
            IEnumerable<CategoryViewModel> categoryViewModelList = new List<CategoryViewModel>();

            if (categoryList != null && categoryList.Count() > 0)
            {
                categoryViewModelList = Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryViewModel>>(categoryList);
            }

            return categoryViewModelList;
        }

        // GET api/values/5
        public CategoryViewModel Get(Guid id)
        {
            var category = CategoryRepository.GetByID(id);
            CategoryViewModel categoryViewModel = Mapper.Map<CategoryViewModel>(category);

            return categoryViewModel;
        }

        // POST api/values
        public CategoryViewModel Post([FromBody]CategoryViewModel value)
        {
            Category category = Mapper.Map<Category>(value);

            if (value.IsNew)
            {
                if (category.CategoryId == Guid.Empty)
                {
                    category.CategoryId = Guid.NewGuid();
                    value.CategoryId = category.CategoryId;
                }
                category = CategoryRepository.Add(category);
            }
            else
            {
                category = CategoryRepository.Update(category);
            }

            return value;
        }

        // DELETE api/values/5
        public IHttpActionResult Delete(Guid id)
        {
            var beerList = BeerRepository.GetByFilter(r => r.CategoryId == id);
            if (beerList != null && beerList.Count() > 0)
            {
                return BadRequest("This category can't be deleted. There have some beers in.");
            }
            else
            {
                CategoryRepository.Delete(id);
            }

            return Ok();
        }
    }
}
