﻿using AutoMapper;
using CE.Entity;
using CE.Repository;
using System;
using System.Collections.Generic;
using System.Web.Http;
using CE.WebApi.Models;
using System.Linq;
using CE.WebApi.Providers;
using Microsoft.AspNet.Identity;
using System.Net.Http;

namespace CE.WebApi.Controllers
{
    [Authorize]
    public class BeerController : BaseApiController
    {
        public BeerController(ISqlServerRepository<Category> categoryRepository, ISqlServerRepository<Beer> beerRepository, ISqlServerRepository<ConsumedBeer> consumedRepository)
            : base(categoryRepository, beerRepository, consumedRepository)
        {
        }

        // GET api/values
        [AllowAnonymous]
        public IEnumerable<BeerViewModel> GetAnonymous(string categoryName = null)
        {
            var beerList = string.IsNullOrWhiteSpace(categoryName)
                ? BeerRepository.GetByFilter(r => !r.IsArchive)
                : BeerRepository.GetByFilter(r => r.Category.Name.IndexOf(categoryName) >= 0 && !r.IsArchive);
            List<BeerViewModel> beerViewModelList = Mapper.Map<List<BeerViewModel>>(beerList);

            return beerViewModelList;
        }

        // GET api/values
        [Authorize(Roles = Common.Constants.ApplicationRole.Customer)]
        public IEnumerable<BeerViewModel> GetConsumed()
        {
            string userId = User.Identity.GetUserId();
            var beerList = ConsumedRepository.GetByFilter(r => r.Id == userId).Select(r => r.Beer);
            List<BeerViewModel> beerViewModelList = Mapper.Map<List<BeerViewModel>>(beerList);

            return beerViewModelList;
        }

        // GET api/values
        [Authorize(Roles = Common.Constants.ApplicationRole.Administrator)]
        public IEnumerable<BeerViewModel> Get()
        {
            IEnumerable<Beer> beerList = BeerRepository.GetAll;
            IEnumerable<BeerViewModel> beerViewModelList = new List<BeerViewModel>();

            if (beerList != null && beerList.Count() > 0)
            {
                beerViewModelList = Mapper.Map<IEnumerable<Beer>, IEnumerable<BeerViewModel>>(beerList);
            }

            return beerViewModelList;
        }

        // GET api/values/5
        [Authorize(Roles = Common.Constants.ApplicationRole.Administrator)]
        public BeerViewModel Get(Guid id)
        {
            var beer = BeerRepository.GetByID(id);
            BeerViewModel beerViewModel = Mapper.Map<BeerViewModel>(beer);

            return beerViewModel;
        }

        // POST api/values
        [Authorize(Roles = Common.Constants.ApplicationRole.Administrator)]
        public BeerViewModel Post([FromBody]BeerViewModel value)
        {
            Beer beer = Mapper.Map<Beer>(value);

            //Update fail in entity framework
            if (beer.Category != null) beer.Category = null;

            if (value.IsNew)
            {
                if (beer.BeerId == Guid.Empty)
                {
                    beer.BeerId = Guid.NewGuid();
                    value.BeerId = beer.BeerId;
                }
                BeerRepository.Add(beer);
            }
            else
            {
                BeerRepository.Update(beer);
            }

            return value;
        }

        // POST api/values
        [Authorize(Roles = Common.Constants.ApplicationRole.Customer)]
        public IHttpActionResult PostConsumed([FromBody]ConsumedBeerViewModel value)
        {
            ConsumedBeer cBeer = new ConsumedBeer();
            cBeer.ConsumedBeerId = Guid.NewGuid();
            cBeer.Id = User.Identity.GetUserId();
            cBeer.BeerId = value.BeerId;
            ConsumedRepository.Add(cBeer);

            return Ok();
        }

        // POST api/values
        [Authorize(Roles = Common.Constants.ApplicationRole.Administrator)]
        public IHttpActionResult Archive([FromBody]ArchiveBeerViewModel value)
        {
            var beer = BeerRepository.GetByID(value.BeerId);
            beer.IsArchive = true;
            BeerRepository.Update(beer);

            return Ok();
        }

        // DELETE api/values/5
        [Authorize(Roles = Common.Constants.ApplicationRole.Administrator)]
        public IHttpActionResult Delete(Guid id)
        {
            bool isSuccess = BeerRepository.Delete(id);

            if (isSuccess)
                return Ok();
            else
                return BadRequest("Can not delete this beer.");
        }
    }
}
