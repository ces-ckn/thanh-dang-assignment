﻿using CE.Entity;
using CE.Repository;
using System.Web.Http;

namespace CE.WebApi.Controllers
{
    public class BaseApiController : ApiController
    {
        public ISqlServerRepository<Category> CategoryRepository;
        public ISqlServerRepository<Beer> BeerRepository;
        public ISqlServerRepository<ConsumedBeer> ConsumedRepository;

        public BaseApiController(ISqlServerRepository<Category> categoryRepository,
           ISqlServerRepository<Beer> beerRepository,
           ISqlServerRepository<ConsumedBeer> consumedRepository)
        {
            CategoryRepository = categoryRepository;
            BeerRepository = beerRepository;
            ConsumedRepository = consumedRepository;
        }
    }
}