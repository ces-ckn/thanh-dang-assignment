﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CE.WebApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Api()
        {
            ViewBag.Title = "API";

            return RedirectToAction("Index", "Help");
        }

        [HttpGet]
        public ActionResult RegisterAsAdmin()
        {
            return PartialView("ManageAdmin");
        }

        [HttpGet]
        public ActionResult RegisterAsCustomer()
        {
            return PartialView("ManageCustomer");
        }

        [HttpGet]
        public ActionResult CategoryListing()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult BeerListing()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult BeerAnonymousListing()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult CustomerZone()
        {
            return PartialView();
        }
    }
}
