﻿using AutoMapper;
using CE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CE.WebApi.Models;

namespace CE.WebApi
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            ConfigureWebMapping();
        }

        private static void ConfigureWebMapping()
        {
            Mapper.CreateMap<Beer, BeerViewModel>()
                .ForMember(d => d.CategoryViewModel, m => m.MapFrom(s => s.Category));
            Mapper.CreateMap<BeerViewModel, Beer>()
                .ForMember(d => d.Category, m => m.MapFrom(s => s.CategoryViewModel));
            Mapper.CreateMap<Category, CategoryViewModel>()
                .ForMember(d => d.BeerViewModelList, m => m.MapFrom(s => s.BeerList));
            Mapper.CreateMap<CategoryViewModel, Category>()
                .ForMember(d => d.BeerList, m => m.MapFrom(s => s.BeerViewModelList));
        }
    }
}