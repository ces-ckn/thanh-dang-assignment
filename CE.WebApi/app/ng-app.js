﻿(function () {
    'use strict';
    var tokenKey = 'accessToken';

    var app = angular.module('app', ['ngRoute', 'ngResource'])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('myHttpInterceptor');
        var spinnerFunction = function (data, headersGetter) {
            // todo start the spinner here
            //alert('start spinner');
            $('#divloading').show();
            $('.error').html('');
            return data;
        };
        $httpProvider.defaults.transformRequest.push(spinnerFunction);
        $httpProvider.defaults.transformResponse.push(function (responseData) {

            return responseData;
        });

        $httpProvider.interceptors.push(['$injector',
        function ($injector) {
            return $injector.get('AuthInterceptor');
        }]);

    }])
    .factory('AuthInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
        return {
            responseError: function (response) {
                $('#divloading').hide();

                if (response.data.error == undefined) {
                    $('.error').html(response.data.Message);
                    //alert(response.data.Message);
                } else {
                    $('.error').html(response.data.error);
                    //alert(response.data.error);
                }
                //console.log(response);

                return $q.reject(response);
            }
        };
    }])
    .factory('myHttpInterceptor', ['$q', '$window', function ($q, $window) {
        return function (promise) {
            return promise.then(function (response) {
                $('#divloading').hide();
                return response;
            }, function (response) {
                $('#divloading').hide();
                return $q.reject(response);
            });
        };
    }])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/Home/RegisterAsAdmin',
            {
                controller: 'AdminController',
                templateUrl: '/Home/RegisterAsAdmin'
            })
            .when('/Home/RegisterAsCustomer',
            {
                controller: 'AdminController',
                templateUrl: '/Home/RegisterAsCustomer'
            })
            .when('/Home/CategoryListing',
            {
                controller: 'CategoryController',
                templateUrl: '/Home/CategoryListing'
            })
            .when('/Home/BeerListing',
            {
                controller: 'BeerController',
                templateUrl: '/Home/BeerListing'
            })
            .when('/Home/BeerAnonymousListing',
            {
                controller: 'CustomerController',
                templateUrl: '/Home/BeerAnonymousListing'
            })
            .when('/Home/CustomerZone',
            {
                controller: 'CustomerController',
                templateUrl: '/Home/CustomerZone'
            })
            .otherwise(
            {
                redirectTo: '/Help/Index'
            });
    }])
    .factory('commnonservice', ['$http', '$q', function ($http, $q) {
        return {
            login: function (path, data, method) {
                var qdefer = $q.defer();

                var option = {
                    method: method,
                    url: path,
                    data: $.param(data),
                    async: false
                };

                $http(option).success(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.resolve(data);
                }).error(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.reject(data);
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    //console.log(data);
                });

                return qdefer.promise;
            },
            //this used for register
            executenonheader: function (path, data, method) {
                var qdefer = $q.defer();

                var option = {
                    method: method,
                    url: path,
                    data: data,
                    async: false
                };
                if (method === 'GET') {
                    option = {
                        method: method,
                        url: path,
                        params: data,
                        async: false
                    };
                }

                $http(option).success(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.resolve(data);
                }).error(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.reject(data);
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    //console.log(data);
                });

                return qdefer.promise;
            },
            execute: function (path, data, method) {
                var qdefer = $q.defer();

                var token = sessionStorage.getItem(tokenKey);
                var headers = {};
                if (token) {
                    headers.Authorization = 'Bearer ' + token;
                }

                var option = {};
                if (data != undefined) {
                    option = {
                        method: method,
                        url: path,
                        data: data,
                        headers: headers,
                        async: false
                    };
                } else {
                    option = {
                        method: method,
                        url: path,
                        headers: headers,
                        async: false
                    };
                }

                $http(option).success(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.resolve(data);
                }).error(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.reject(data);
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    //console.log(data);
                });

                return qdefer.promise;
            },
            executedelete: function (path, Id) {
                var qdefer = $q.defer();

                var token = sessionStorage.getItem(tokenKey);
                var headers = {};
                if (token) {
                    headers.Authorization = 'Bearer ' + token;
                }

                $http.delete(path + Id, {
                    headers: headers
                })
                .success(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.resolve(data);
                }).error(function (data, status, headers, config) {
                    $('#divloading').hide();
                    qdefer.reject(data);
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    //console.log(data);
                });

                return qdefer.promise;
            }
        }
    }])
    .factory('admins', ['$http', '$q', 'commnonservice', function ($http, $q, commnonservice) {
        return {
            registeradmin: function (entity) {
                var path = '/api/Account/RegisterAsAdmin/';
                return commnonservice.executenonheader(path, entity, 'POST');
            },
            registercustomer: function (entity) {
                var path = '/api/Account/RegisterAsCustomer/';
                return commnonservice.executenonheader(path, entity, 'POST');
            },
            loginadmin: function (entity) {
                var path = '/Token';
                return commnonservice.login(path, entity, 'POST');
            },
            logincustomer: function (entity) {
                var path = '/Token';
                return commnonservice.login(path, entity, 'POST');
            }
        }
    }])
    .factory('categories', ['$http', '$q', 'commnonservice', function ($http, $q, commnonservice) {
        return {
            getcategories: function () {
                var path = '/api/Category/Get/';
                return commnonservice.execute(path, undefined, 'GET');
            },
            addcategory: function (entity) {
                var path = '/api/Category/Post/';
                return commnonservice.execute(path, entity, 'POST');
            },
            updatecategory: function (entity) {
                var path = '/api/Category/Post/';
                return commnonservice.execute(path, entity, 'POST');
            },
            deletecategory: function (id) {
                var path = '/api/Category/Delete/';
                return commnonservice.executedelete(path, id);
            }
        }
    }])
    .factory('beers', ['$http', '$q', 'commnonservice', function ($http, $q, commnonservice) {
        return {
            getcategories: function () {
                var path = '/api/Category/Get/';
                return commnonservice.execute(path, undefined, 'GET');
            },
            getbeers: function () {
                var path = '/api/Beer/Get/';
                return commnonservice.execute(path, undefined, 'GET');
            },
            getanonymousbeers: function (categoryname) {
                var path = '/api/Beer/GetAnonymous/';
                return commnonservice.executenonheader(path, { categoryName: categoryname }, 'GET'); //Remove header to get anonymous
            },
            getconsumedbeers: function () {
                var path = '/api/Beer/GetConsumed/';
                return commnonservice.execute(path, undefined, 'GET');
            },
            addbeer: function (entity) {
                var path = '/api/Beer/Post/';
                return commnonservice.execute(path, entity, 'POST');
            },
            addconsumedbeer: function (entity) {
                var path = '/api/Beer/PostConsumed/';
                return commnonservice.execute(path, entity, 'POST');
            },
            updatebeer: function (entity) {
                var path = '/api/Beer/Post/';
                return commnonservice.execute(path, entity, 'POST');
            },
            archivebeer: function (entity) {
                var path = '/api/Beer/Archive/';
                return commnonservice.execute(path, entity, 'POST');
            },
            deletebeer: function (id) {
                var path = '/api/Beer/Delete/';
                return commnonservice.executedelete(path, id);
            }
        }
    }])
    .controller('AdminController', ['$http', '$scope', 'admins', '$rootScope', function ($http, $scope, admins, $rootScope) {
        $scope.entity = {};
        $scope.entitylogin = { Email: 'admin@gmail.com', Password: 'abcde12345-' };
        $scope.entitycustomerlogin = { Email: 'customer@gmail.com', Password: 'abcde12345-' };

        $scope.registeradmin = function () {
            $scope.loading = true;
            admins.registeradmin($scope.entity).then(function (entity) {
                alert("Register successfully!");
                //$scope.loading = false;
            });
            $scope.loading = true;
        }
        $scope.registercustomer = function () {
            $scope.loading = true;
            admins.registercustomer($scope.entity).then(function (data) {
                alert("Register successfully!");
                //$scope.loading = false;
            });
        }
        $scope.loginadmin = function () {
            $scope.loading = true;
            var data = {
                grant_type: 'password',
                username: $scope.entitylogin.Email,
                Password: $scope.entitylogin.Password
            };
            admins.loginadmin(data).then(function (data) {
                alert("Login successfully!");
                //self.user(data.userName);
                // Cache the access token in session storage.
                sessionStorage.setItem(tokenKey, data.access_token);
            });
        }
        $scope.logincustomer = function () {
            $scope.loading = true;
            var data = {
                grant_type: 'password',
                username: $scope.entitycustomerlogin.Email,
                Password: $scope.entitycustomerlogin.Password
            };
            admins.logincustomer(data).then(function (data) {
                alert("Login successfully!");
                //self.user(data.userName);
                // Cache the access token in session storage.
                sessionStorage.setItem(tokenKey, data.access_token);
                //$scope.loading = false;
            });
        }
    }])
    .controller('CategoryController', ['$http', '$scope', 'categories', function ($http, $scope, categories) {
        //declare variable for mainain ajax load and entry or edit mode
        $scope.loading = true;
        $scope.categories = {};
        $scope.category = { editMode: false };
        $scope.newcategory = { addMode: false };

        function LoadCategoryList() {
            categories.getcategories().then(function (result) {
                if (result != undefined && result.length > 0) {
                    $scope.categories = result;
                    $scope.loading = false;
                }
            });
        }
        LoadCategoryList();

        //by pressing toggleEdit button ng-click in html, this method will be hit
        $scope.toggleEdit = function (category) {
            category.editMode = !category.editMode;
        };

        //by pressing toggleAdd button ng-click in html, this method will be hit
        $scope.toggleAdd = function () {
            $scope.newcategory.addMode = !$scope.newcategory.addMode;
        };

        //Insert Category
        $scope.add = function () {
            $scope.loading = true;
            categories.addcategory($scope.newcategory).then(function (data) {
                alert("Added Successfully!!");
                $scope.newcategory.addMode = false;
                $scope.categories.push(data);
                $scope.loading = false;
            });
        }

        //Edit Category
        $scope.save = function (category) {
            $scope.loading = true;
            category.IsNew = false;
            var categoryT = category;
            categories.updatecategory(categoryT).then(function (data) {
                alert("Saved Successfully!!");
                categoryT.editMode = false;
                $scope.loading = false;
            });
        }

        //Delete Category
        $scope.deletecategory = function (category) {
            $scope.loading = true;
            var Id = category.CategoryId;
            categories.deletecategory(Id).then(function (data) {
                alert("Deleted Successfully!!");
                $.each($scope.categories, function (i) {
                    if ($scope.categories[i].CategoryId === Id) {
                        $scope.categories.splice(i, 1);
                        return false;
                    }
                });
                $scope.loading = false;
            });
        }
    }])
    .controller('BeerController', ['$http', '$scope', 'categories', 'beers', function ($http, $scope, categories, beers) {
        //declare variable for mainain ajax load and entry or edit mode
        $scope.loading = true;
        $scope.categories = {};
        $scope.beers = {};
        $scope.beer = { editMode: false };
        $scope.newbeer = { addMode: false };

        function LoadCategoryList() {
            categories.getcategories().then(function (result) {
                if (result != undefined && result.length > 0) {
                    $scope.categories = result;
                    $scope.loading = false;
                }
            });
        }
        LoadCategoryList();

        function LoadBeerList() {
            beers.getbeers().then(function (result) {
                if (result != undefined && result.length > 0) {
                    $scope.beers = result;
                    $scope.loading = false;
                }
            });
        }
        LoadBeerList();

        //by pressing toggleEdit button ng-click in html, this method will be hit
        $scope.toggleEdit = function (beer) {
            beer.editMode = !beer.editMode;
        };

        //by pressing toggleAdd button ng-click in html, this method will be hit
        $scope.toggleAdd = function () {
            $scope.newbeer.addMode = !$scope.newbeer.addMode;
        };

        //Insert Beer
        $scope.add = function () {
            $scope.loading = true;
            var selectedCategory = null;
            for (var i = 0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].CategoryId === $scope.newbeer.CategoryId) {
                    selectedCategory = $scope.categories[i];
                    break;
                }
            }
            beers.addbeer($scope.newbeer).then(function (data) {
                alert("Added Successfully!!");
                $scope.newbeer.addMode = false;
                data.CategoryViewModel = selectedCategory;
                $scope.beers.push(data);
                $scope.loading = false;
            });
        }

        //Edit Beer
        $scope.save = function (beer) {
            $scope.loading = true;
            var selectedCategory = null;
            for (var i = 0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].CategoryId === beer.CategoryId) {
                    selectedCategory = $scope.categories[i];
                    break;
                }
            }
            beer.IsNew = false;
            var beerT = beer;
            beers.updatebeer(beerT).then(function (data) {
                alert("Saved Successfully!!");
                beerT.CategoryViewModel = selectedCategory;
                beerT.editMode = false;
                $scope.loading = false;
            });
        }

        //Archive Beer
        $scope.archivebeer = function (beer) {
            $scope.loading = true;
            beers.archivebeer({ BeerId: beer.BeerId }).then(function (data) {
                alert("Archived Successfully!!");
                beer.IsArchive = true;
                $scope.loading = false;
            });
        }

        //Delete Beer
        $scope.deletebeer = function (beer) {
            $scope.loading = true;
            var Id = beer.BeerId;
            beers.deletebeer(Id).then(function (data) {
                alert("Deleted Successfully!!");
                $.each($scope.beers, function (i) {
                    if ($scope.beers[i].BeerId === Id) {
                        $scope.beers.splice(i, 1);
                        return false;
                    }
                });
                $scope.loading = false;
            });
        }
    }])
    .controller('CustomerController', ['$http', '$scope', 'beers', function ($http, $scope, beers) {
        //declare variable for mainain ajax load and entry or edit mode
        $scope.loading = true;
        $scope.beers = {};
        $scope.consumedbeers = {};
        $scope.newconsumedbeer = {};
        $scope.categoryname = '';

        function GetBeerListing() {
            beers.getanonymousbeers($scope.categoryname).then(function (result) {
                if (result != undefined && result.length > 0) {
                    $scope.beers = result;
                }
            });
        }
        GetBeerListing();

        function GetConsumedBeerListing() {
            beers.getconsumedbeers().then(function (result) {
                if (result != undefined && result.length > 0) {
                    $scope.consumedbeers = result;
                    $scope.loading = false;
                }
            });
        }
        GetConsumedBeerListing();

        //Insert Consumed Beer
        $scope.add = function () {
            $scope.loading = true;
            beers.addconsumedbeer($scope.newconsumedbeer).then(function (data) {
                alert("Added Successfully!!");
                $scope.loading = true;
                GetConsumedBeerListing();
                $scope.loading = false;
            });
        }

        //Get Anonymous Beer
        $scope.getanonymousbeers = function () {
            $scope.loading = true;
            GetBeerListing();
            $scope.loading = false;
        }
    }])
})();