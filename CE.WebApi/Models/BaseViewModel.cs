﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CE.WebApi.Models
{
    public class BaseViewModel
    {
        public BaseViewModel()
        {
            IsNew = true;
        }

        public bool IsNew { get; set; }
    }
}