﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CE.WebApi.Models
{
    public class CategoryViewModel : BaseViewModel
    {
        public Guid CategoryId { get; set; }

        public string Name { get; set; }

        public List<BeerViewModel> BeerViewModelList { get; set; }

        public bool CanDelete { get { return BeerViewModelList == null || BeerViewModelList.Count == 0; } }
    }
}