﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CE.WebApi.Models
{
    public class BeerViewModel : BaseViewModel
    {
        public Guid BeerId { get; set; }

        public string Manufacturer { get; set; }

        public string Name { get; set; }

        public Guid CategoryId { get; set; }
        public CategoryViewModel CategoryViewModel { get; set; }

        public string Country { get; set; }

        public string Price { get; set; }

        public string Description { get; set; }

        public bool IsArchive { get; set; }
    }
}