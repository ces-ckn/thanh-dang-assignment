﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CE.WebApi.Models
{
    public class ConsumedBeerViewModel
    {
        public Guid BeerId { get; set; }
    }
}