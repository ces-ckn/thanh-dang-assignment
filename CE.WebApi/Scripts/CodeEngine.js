﻿var tokenKey = 'accessToken';

$(document).ready(function () {
    $('#btnRegisterAsAdmin').click(function () {
        var data = {
            Email: $('#Email').val(),
            Password: $('#Password').val(),
            ConfirmPassword: $('#ConfirmPassword').val(),
        };

        $.ajax({
            type: 'POST',
            url: '/api/Account/RegisterAsAdmin',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data)
        }).done(function (data) {
            alert("Register successfully!");
        }).fail(function (data) {
            alert(data.responseText);
        });
    });

    $('#btnLoginAsAdmin').click(function () {
        var data = {
            grant_type: 'password',
            username: $('#txtLoginEmail').val(),
            Password: $('#txtLoginPassword').val()
        };

        $.ajax({
            type: 'POST',
            url: '/Token',
            data: data
        }).done(function (data) {
            alert("Login successfully!");
            //self.user(data.userName);
            // Cache the access token in session storage.
            sessionStorage.setItem(tokenKey, data.access_token);
        }).fail(function (data) {
            alert(data.responseText);
        });
    });

    $('#btnRegisterAsCustomer').click(function () {
        var data = {
            Email: $('#Email').val(),
            Password: $('#Password').val(),
            ConfirmPassword: $('#ConfirmPassword').val(),
        };

        $.ajax({
            type: 'POST',
            url: '/api/Account/RegisterAsCustomer',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data)
        }).done(function (data) {
            alert("Register successfully!");
        }).fail(function (data) {
            alert(data.responseText);
        });
    });

    $('#btnLoginAsCustomer').click(function () {
        var data = {
            grant_type: 'password',
            username: $('#txtLoginEmail').val(),
            Password: $('#txtLoginPassword').val()
        };

        $.ajax({
            type: 'POST',
            url: '/Token',
            data: data
        }).done(function (data) {
            alert("Login successfully!");
            //self.user(data.userName);
            // Cache the access token in session storage.
            sessionStorage.setItem(tokenKey, data.access_token);
        }).fail(function (data) {
            alert(data.responseText);
        });
    });

    //// If we already have a bearer token, set the Authorization header.
    //var token = sessionStorage.getItem(tokenKey);
    //var headers = {};
    //if (token) {
    //    headers.Authorization = 'Bearer ' + token;
    //}

    //$.ajax({
    //    type: 'GET',
    //    url: 'api/values/1',
    //    headers: headers
    //}).done(function (data) {
    //    self.result(data);
    //}).fail(function (data) {
    //    alert(data.responseText);
    //});
});

(function () {
    'use strict';
    //create angularjs controller
    var app = angular.module('app', []);//set and get the angular module
    app.controller('categoryController', ['$scope', '$http', categoryController]);
    app.controller('beerController', ['$scope', '$http', beerController]);
    app.controller('customerController', ['$scope', '$http', customerController]);

    // If we already have a bearer token, set the Authorization header.
    var token = sessionStorage.getItem(tokenKey);
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    //angularjs controller method
    function categoryController($scope, $http) {

        //declare variable for mainain ajax load and entry or edit mode
        $scope.loading = true;
        $scope.addMode = false;

        function LoadCategoryList() {
            $http({
                method: 'GET',
                url: '/api/Category/Get/',
                headers: headers
            }).success(function (data, status, headers, config) {
                $scope.categorys = data;
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while loading category list! " + data.Message;
                $scope.loading = false;
            });
        }
        LoadCategoryList();

        //by pressing toggleEdit button ng-click in html, this method will be hit
        $scope.toggleEdit = function () {
            this.category.editMode = !this.category.editMode;
        };

        //by pressing toggleAdd button ng-click in html, this method will be hit
        $scope.toggleAdd = function () {
            $scope.addMode = !$scope.addMode;
        };

        //Insert Category
        $scope.add = function () {
            $scope.loading = true;
            $http({
                method: 'POST',
                url: '/api/Category/Post/',
                data: this.newcategory,
                headers: headers
            }).success(function (data, status, headers, config) {
                alert("Added Successfully!!");
                $scope.addMode = false;
                $scope.categorys.push(data);
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while Adding Category! " + data;
                $scope.loading = false;
            });
        };

        //Edit Category
        $scope.save = function () {
            $scope.loading = true;
            this.category.IsNew = false;
            var categoryT = this.category;
            $http({
                method: 'POST',
                url: '/api/Category/Post/',
                data: categoryT,
                headers: headers
            }).success(function (data, status, headers, config) {
                alert("Saved Successfully!!");
                categoryT.editMode = false;
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while Saving category! " + data;
                $scope.loading = false;
            });
        };

        //Delete Category
        $scope.deletecategory = function () {
            $scope.loading = true;
            var Id = this.category.CategoryId;
            $http.delete('/api/Category/Delete/' + Id, {
                headers: headers
            }).success(function (data) {
                alert("Deleted Successfully!!");
                $.each($scope.categorys, function (i) {
                    if ($scope.categorys[i].CategoryId === Id) {
                        $scope.categorys.splice(i, 1);
                        return false;
                    }
                });
                $scope.loading = false;
            }).error(function (data) {
                $scope.error = "An Error has occured while Deleting Category! " + data.Message;
                $scope.loading = false;
            });
        };
    }

    //angularjs controller method
    function beerController($scope, $http) {

        //declare variable for mainain ajax load and entry or edit mode
        $scope.loading = true;
        $scope.addMode = false;
        $scope.categories = [];

        $http({
            method: 'GET',
            url: '/api/Category/Get/',
            headers: headers
        }).success(function (data, status, headers, config) {
            $scope.categories = data;
        }).error(function (data, status, headers, config) {
            $scope.error = "An error has occured while loading category list! " + data.Message;
        });

        function GetBeerListing() {
            $http({
                method: 'GET',
                url: '/api/Beer/Get/',
                headers: headers
            }).success(function (data, status, headers, config) {
                $scope.beers = data;
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while loading beer list! " + data.Message;
                $scope.loading = false;
            });
        }
        GetBeerListing();

        //by pressing toggleEdit button ng-click in html, this method will be hit
        $scope.toggleEdit = function () {
            this.beer.editMode = !this.beer.editMode;
        };

        //by pressing toggleAdd button ng-click in html, this method will be hit
        $scope.toggleAdd = function () {
            $scope.addMode = !$scope.addMode;
        };

        //Insert Beer
        $scope.add = function () {
            $scope.loading = true;
            var selectedCategory = null;
            for (var i = 0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].CategoryId === this.newbeer.CategoryId) {
                    selectedCategory = $scope.categories[i];
                    break;
                }
            }
            $http({
                method: 'POST',
                url: '/api/Beer/Post/',
                data: this.newbeer,
                headers: headers
            }).success(function (data, status, headers, config) {
                alert("Added Successfully!!");
                $scope.addMode = false;
                data.CategoryViewModel = selectedCategory;
                $scope.beers.push(data);
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while Adding Beer! " + data;
                $scope.loading = false;
            });
        };

        //Edit Beer
        $scope.save = function () {
            $scope.loading = true;
            var selectedCategory = null;
            for (var i = 0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].CategoryId === this.beer.CategoryId) {
                    selectedCategory = $scope.categories[i];
                    break;
                }
            }
            this.beer.IsNew = false;
            var beerT = this.beer;
            $http({
                method: 'POST',
                url: '/api/Beer/Post/',
                data: beerT,
                headers: headers
            }).success(function (data, status, headers, config) {
                alert("Saved Successfully!!");
                beerT.CategoryViewModel = selectedCategory;
                beerT.editMode = false;
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while Saving beer! " + data;
                $scope.loading = false;
            });
        };

        //Delete Beer
        $scope.deletebeer = function () {
            $scope.loading = true;
            var Id = this.beer.BeerId;
            $http.delete('/api/Beer/Delete/' + Id, {
                headers: headers
            }).success(function (data) {
                alert("Deleted Successfully!!");
                $.each($scope.beers, function (i) {
                    if ($scope.beers[i].BeerId === Id) {
                        $scope.beers.splice(i, 1);
                        return false;
                    }
                });
                $scope.loading = false;
            }).error(function (data) {
                $scope.error = "An Error has occured while Deleting Beer! " + data;
                $scope.loading = false;
            });
        };
    }

    //angularjs controller method
    function customerController($scope, $http) {

        //declare variable for mainain ajax load and entry or edit mode
        $scope.loading = true;
        $scope.beers = [];
        $scope.consumedbeers = [];

        function GetBeerListing() {
            $http({
                method: 'GET',
                url: '/api/Beer/GetAnonymous/',
                headers: headers
            }).success(function (data, status, headers, config) {
                $scope.beers = data;
            }).error(function (data, status, headers, config) {
                $scope.error = "An error has occured while loading beer list! " + data.Message;
            });
        }
        GetBeerListing();

        function GetConsumedBeerListing() {
            $http({
                method: 'GET',
                url: '/api/Beer/GetConsumed/',
                headers: headers
            }).success(function (data, status, headers, config) {
                $scope.consumedbeers = data;
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while loading consumed beer list! " + data.Message;
                $scope.loading = false;
            });
        }
        GetConsumedBeerListing();

        //Insert Consumed Beer
        $scope.add = function () {
            $scope.loading = true;
            $http({
                method: 'POST',
                url: '/api/Beer/PostConsumed/',
                data: this.newconsumedbeer,
                headers: headers
            }).success(function (data, status, headers, config) {
                alert("Added Successfully!!");
                $scope.loading = true;
                GetConsumedBeerListing();
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                $scope.error = "An Error has occured while Adding Consumed Beer! " + data;
                $scope.loading = false;
            });
        };
    }
})();